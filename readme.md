##task 2

Zaproponuj definicję funkcji getLongestString(),
która przyjmuje dowolną liczbę parametrów 
i zwraca najdłuższy string z podanych argumentów. 
Jeżeli jakikolwiek argument nie będzie typu string, 
funkcja powinna rzucić wyjątkiem. 
Udokumentuj funkcję używając docblock 
oraz zaproponuj zestaw testów zapewniający prawidłowość 
działania funkcji.

##Quick start
- ściągasz repo
- composer install
- composer dump
- folder projektu musi być objęty virtualnym serverem, np. apache
- wyniki oglądasz w przeglądarce
###
##Testy
Reguły testu są zdefioniowane tu: phpunit.xml.dist
```
phpunit/phpunit": "7.4
```