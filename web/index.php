<?php

use Services\LongestStringResolver\LongestStringResolver;

require '../vendor/autoload.php';

function generateString() {
    return 'base_string_'.random_bytes(rand(1,10));
}

$randomString = [
    generateString(),
    generateString(),
    generateString(),
    generateString(),
];

echo '<p>task 2</p>';
$longestStringResolver = new LongestStringResolver();
/** @var StringDTO $maxStringDTO */
$maxStringDTO = $longestStringResolver->longestStringResolver($randomString);

echo '<p>String to compare</p>';
var_dump($randomString);
echo '<p>Longest string is:</p>';
var_dump($maxStringDTO);
