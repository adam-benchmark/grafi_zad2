<?php

namespace Tests;

use PHPUnit\Framework\TestCase;

class PHPUnitUtil extends TestCase
{
    /**
     * @param $obj
     * @param $name
     * @param array $args
     *
     * @return mixed
     *
     * @throws \ReflectionException
     */
    public static function getPrivateMethod($obj, $name, array $args)
    {
        $class = new \ReflectionClass($obj);
        $method = $class->getMethod($name);
        $method->setAccessible(true);

        return  $method->invokeArgs($obj, $args);
    }

}
