<?php

namespace Services\LongestStringResolver;

use DTO\StringDTO;
use Tests\PHPUnitUtil;

class FinderLongestStringTest extends PHPUnitUtil
{
    /**
     * @dataProvider providerFindLongestString
     */
    public function testFindLongestString($expected, $input)
    {
        $finderLongestString = new FinderLongestString();

        /** @var StringDTO $result */
        $result = $finderLongestString->findLongestString($input);
        $this->assertEquals($expected, $result->content);
    }

    /**
     * @return array
     */
    public function providerFindLongestString()
    {
        return [
            ['defg', ['abc', 'defg', 'ab']],
            ['abcxyz', ['abcxyz', 'de', 'abc']],
            ['abcxyz12 3', ['abcxyz12 3', 'de1 2', 'abc 12']],
        ];
    }

    /**
     * @dataProvider providerFindLongestStringForExceptoin
     *
     * @expectedException \InvalidArgumentException
     */
    public function testThrowException($expected, $input)
    {
        $finderLongestString = new FinderLongestString();

        /** @var StringDTO $result */
        $result = $finderLongestString->findLongestString($input);

        $this->expectException(\InvalidArgumentException::class);
    }

    /**
     * @return array
     */
    public function providerFindLongestStringForExceptoin()
    {
        return [
            [123, [123, 'defghi', 'abc']],
        ];
    }

    /**
     * @dataProvider providerCheckIfString
     */
    public function testCheckIfString($expected, $input)
    {
        $finderLongestString = new FinderLongestString();
        /** @var \ReflectionClass $result */
        $result = PHPUnitUtil::getPrivateMethod(
            $finderLongestString,
            'checkIfString',
            [$input]
        );

        $this->assertEquals($expected, $result);
    }

    /**
     * @return array
     */
    public function providerCheckIfString()
    {
        return [
            [true, 'abc'],
            [true, 'abc 123'],
        ];
    }

    /**
     * @dataProvider providerCheckIfStringThrowException
     * @expectedException \InvalidArgumentException
     */
    public function testCheckIfStringThrowException($expected, $input)
    {
        /** @var \ReflectionClass $result */
        $result = PHPUnitUtil::getPrivateMethod(
            new FinderLongestString(),
            'checkIfString',
            [$input]
        );

        $this->expectException(\InvalidArgumentException::class);
    }

    /**
     * @return array
     */
    public function providerCheckIfStringThrowException()
    {
        return [
            [true, 123],
        ];
    }

    /**
     * @dataProvider providerTestGetLengthData
     */
    public function testGetLength($expected, $input)
    {
        /** @var \ReflectionClass $result */
        $result = PHPUnitUtil::getPrivateMethod(
            new FinderLongestString(),
            'getLength',
            [$input]
        );

        $this->assertEquals($expected, $result);
    }

    /**
     * @return array
     */
    public function providerTestGetLengthData()
    {
        return [
            [2, 'ab'],
            [3, 'abc'],
            [5, 'abc d'],
        ];
    }

    /**
     * @dataProvider provideerTestCheckIfNewStringIsBiggerWithMock
     */
    public function testCheckIfNewStringIsBiggerWithMock($expected, $input)
    {
        /** @var FinderLongestString $finderLongestString */
        $finderLongestString = $this->setUpMaxStringDTOForTestCheckIfNewStringIsBiggerWithMock($expected);
        /** @var StringDTO $result */
        $result = $finderLongestString->findLongestString($input);
        $this->assertEquals($expected, $result);
    }

    /**
     * @param StringDTO $value
     * @return FinderLongestString
     * @throws \ReflectionException
     */
    private function setUpMaxStringDTOForTestCheckIfNewStringIsBiggerWithMock(StringDTO $value)
    {
        $stringDTO = $value;
        $finderLongestString = new FinderLongestString();
        $reflection = new \ReflectionClass($finderLongestString);
        $reflection_property = $reflection->getProperty('maxStringDTO');
        $reflection_property->setAccessible(true);
        $reflection_property->setValue($finderLongestString, $stringDTO);

        return $finderLongestString;
    }

    /**
     * @return array
     */
    public function provideerTestCheckIfNewStringIsBiggerWithMock()
    {
        return [
            [
                StringDTO::createFromArray(['content' => 'abcd', 'length' => '4']),
                [
                    'abcd',
                    'ax'
                ]
            ]
        ];
    }


}
