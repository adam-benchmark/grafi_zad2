<?php

namespace Services\LongestStringResolver;

use DTO\StringDTO;
use Tests\PHPUnitUtil;

class LongestStringResolverTest extends PHPUnitUtil
{
    /**
     * @dataProvider providerLongestStringResolver
     */
    public function testLongestStringResolver($expected, $input)
    {
        $longestStringResolver = new LongestStringResolver();

        $result = $longestStringResolver->longestStringResolver($input);

        $this->assertEquals($expected, $result->content);
        $this->assertEquals(strlen($expected), $result->length);
        $this->assertInstanceOf(StringDTO::class, $result);
    }

    /**
     * @return array
     */
    public function providerLongestStringResolver()
    {
        return [
          ['abcde', ['abc', 'abcd', 'abcde']]
        ];
    }
}
