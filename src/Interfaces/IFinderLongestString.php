<?php

namespace Interfaces;

use DTO\StringDTO;

abstract class IFinderLongestString
{
    /** @var StringDTO */
    protected $maxStringDTO;

    /**
     * @param $input
     * @return mixed
     */
    abstract protected function checkIfString($input);

    /**
     * @param string $input
     * @return int
     */
    abstract protected function getLength(string $input) : int;

    /**
     * @param StringDTO $currentStringDTO
     *
     * @return mixed
     */
    abstract protected function checkIfNewStringIsBigger(StringDTO $currentStringDTO);

    /**
     * @param StringDTO $maxStringDTO
     */
    protected function setUpFirstValue(StringDTO $maxStringDTO)
    {
        $this->maxStringDTO = $maxStringDTO;
    }

    /**
     * @param $input
     */
    protected function process($input)
    {
        $this->checkIfString($input);

        $currentStringDTO = StringDTO::createFromArray([
            'content' => $input,
            'length' => $this->getLength($input),
        ]);

        $this->checkIfNewStringIsBigger($currentStringDTO);

    }
    /**
     * @param string $input
     * @param string $length
     *
     * @return StringDTO
     */
    protected function setUpNewMaxString(string $input, string $length)
    {
        return StringDTO::createFromArray([
           'content' => $input,
           'length' => $length,
        ]);
    }
}