<?php

namespace Services\LongestStringResolver;

use DTO\StringDTO;
use Interfaces\IFinderLongestString;

class FinderLongestString extends IFinderLongestString
{
    /**
     * FinderLongestString constructor.
     */
    public function __construct()
    {
        $this->setUpFirstValue(StringDTO::createFromArray([
            'content' => '',
            'length' => '0',
        ]));
    }

    /**
     * @param array $input
     *
     * @return StringDTO
     */
    public function findLongestString(array $input)
    {
        foreach ($input as $currentElement) {
            $this->process($currentElement);
        }

        return $this->maxStringDTO;
    }

    /**
     * @param $input
     *
     * @return bool
     *
     * @throws \Exception
     */
    protected function checkIfString($input)
    {
        $result = is_string($input);

        if (!$result) {
            throw new \InvalidArgumentException($input . ' is not a string type!');
        }

        return $result;
    }

    /**
     * @param string $input
     *
     * @return int
     */
    protected function getLength(string $input): int
    {
        return strlen($input);
    }

    /**
     * @param StringDTO $currentStringDTO
     *
     * @return mixed|void
     */
    protected function checkIfNewStringIsBigger(StringDTO $currentStringDTO)
    {
        if ($currentStringDTO->length > $this->maxStringDTO->length) {
            $this->maxStringDTO = $this->setUpNewMaxString($currentStringDTO->content, $currentStringDTO->length);
        }
    }
}