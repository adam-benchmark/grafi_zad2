<?php

namespace Services\LongestStringResolver;

use DTO\StringDTO;

class LongestStringResolver
{
    /**
     * @param array $input
     *
     * @return StringDTO
     */
    public function longestStringResolver(array $input)
    {
        /** @var StringDTO $stringDTO */
        $stringDTO = StringDTO::createFromArray([]);
        if (is_array($input)) {
            $finderLongestString = new FinderLongestString();
            /** @var StringDTO $stringDTO */
            $stringDTO = $finderLongestString->findLongestString($input);
        }

        return $stringDTO;
    }
}