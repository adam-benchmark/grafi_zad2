<?php

namespace DTO;

class StringDTO
{
    /** @var string */
    public $content;
    /** @var string */
    public $length;

    /**
     * @param array $input
     *
     * @return StringDTO
     */
    public static function createFromArray(array $input) : StringDTO
    {
        $dto = new self();
        $dto->content = !empty($input['content']) ? $input['content'] : '';
        $dto->length = !empty($input['length']) ? $input['length'] : '';

        return $dto;
    }
}